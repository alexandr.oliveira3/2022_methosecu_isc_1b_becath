# 2022_MethoSecu_ISC_1b_BeCaTh

## Statement

The president’s cat was kidnapped by separatists. A suspect carrying a USB key has been arrested. Berthier, once again you have to save the Republic! Analyze this key and find out in which city the cat is retained!

Input the city name in lowercase.
